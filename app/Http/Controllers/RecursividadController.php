<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecursividadController extends Controller
{
    public function recursividad($Nfin, $N){
        $N += 1;
        if($N<=$Nfin){
            echo $N, '<br>';
            $this->recursividad($Nfin, $N);
        }
    }
    public function Incrementable(){
        $this->recursividad(12, 0);
        $this->recursividad(100, 13);
    }
}

